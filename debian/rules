#!/usr/bin/make -f
# -*- makefile -*-
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# Make sure build is always done in a reproducible and working
# language environment
LC_ALL = C
export LC_ALL

TMP_BUILD     = tmp-build
language_code = da
country_code  = DK
name_en       = danish
name_da       = dansk

%:
	dh $@


override_dh_auto_build:
	make all
	mkdir -p $(TMP_BUILD)

# --------- idanish ------------------
	cp ispell/dansk.aff   $(TMP_BUILD)/danish.aff
	gzip -9n -c ispell/words-$(language_code).complete \
		> $(TMP_BUILD)/$(name_en).mwl.gz

# --------- myspell-da -------------
#       da_DK.{aff,dic} available in myspell dir. Nothing extra to do here.

# --------- aspell-da -----------------
	cat new_aspell/$(language_code).cwl \
		| gzip -9n -c \
		> $(TMP_BUILD)/$(language_code).cwl.gz

# --------- wdanish ------------------
	cat words-$(language_code) \
		| iconv -f ISO-8859-1 -t UTF-8 \
		| sort -u \
		> $(TMP_BUILD)/danish

# --------- documentation ------------
	iconv -f ISO-8859-1 -t UTF-8 < README > $(TMP_BUILD)/README
	iconv -f ISO-8859-1 -t UTF-8 < contributors > $(TMP_BUILD)/contributors

override_dh_auto_clean:
	rm -rf $(TMP_BUILD)

	dh_auto_clean

override_dh_auto_install:
#       Most files are installed through debian/*.install files
	installdeb-ispell   -pidanish
	installdeb-aspell   -paspell-da
	installdeb-hunspell -pmyspell-da
	installdeb-myspell  -pmyspell-da --bdic-only --srcdir=./myspell
	installdeb-wordlist -pwdanish
	dh_installdocs -A $(TMP_BUILD)/README $(TMP_BUILD)/contributors
